OdontogramApp.directive('oTeeth', function() {
    return {
        restrict: 'AEC',
        transclude: true,
        replace:true,
        scope: {
          teeth: '=',
          action: '=',
          label: '=',
          labelPos: '=',
          type: '=',
          posX: '=',
          posY: '=',
          clickCallback: '='
        },
        // template: '<div style="float:left; height:115px;width:50px; text-align:center"><div ng-show="labelPos == \'top\'" style="height:20px">{{teeth.erupt}}</div><div  ng-show="labelPos == \'top\'" style="height:20px">{{label}}</div><svg  ng-show="labelPos == \'bottom\'" width="50" height="25"><polygon points="12.5,25 25,0, 37.5,25, 12.5,25" style="fill:{{teeth.rct ? \'black\' : \'white\'}};stroke:{{teeth.rct || teeth.nvt ? \'black\' : \'white\'}};stroke-width:1"></polygon> <polygon ng-show="teeth.arrowRight" ng-click="teeth.arrowRight = !teeth.arrowRight" points="10,12.5 40,12.5 38,10 38,15 40,12.5 " style="stroke:black;stroke-width:1"/>  <polygon ng-show="teeth.arrowLeft" ng-click="teeth.arrowLeft = !teeth.arrowLeft" points="40,12.5 10,12.5 12,10 12,15 10,12.5 " style="stroke:black;stroke-width:1"/> </svg><div style="height:0px; width:0px; float:left"><svg height="5" width="5" xmlns="http://www.w3.org/2000/svg" version="1.1"> <defs> <pattern id="lightstripe" patternUnits="userSpaceOnUse" width="5" height="5"> <image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPSc1JyBoZWlnaHQ9JzUnPgogIDxyZWN0IHdpZHRoPSc1JyBoZWlnaHQ9JzUnIGZpbGw9J3doaXRlJy8+CiAgPHBhdGggZD0nTTAgNUw1IDBaTTYgNEw0IDZaTS0xIDFMMSAtMVonIHN0cm9rZT0nIzg4OCcgc3Ryb2tlLXdpZHRoPScxJy8+Cjwvc3ZnPg==" x="0" y="0" width="5" height="5"> </image> </pattern> </defs> </svg></div><svg height="50" width="50"><polygon ng-click="click(\'left\')" points="15,15 15,35 0,50 0,0" style="fill:{{color[teeth.left.fillColor]}};stroke:black;stroke-width:{{teeth.left.strokeWidth}}"/> <polygon ng-click="click(\'top\')" points="15,15 35,15 50,0 0,0" style="fill:{{color[teeth.top.fillColor]}};stroke:black;stroke-width:{{teeth.top.strokeWidth}}"/> <polygon ng-click="click(\'right\')" points="35,15 35,35 50,50 50,0" style="fill:{{color[teeth.right.fillColor]}};stroke:black;stroke-width:{{teeth.right.strokeWidth}}"/> <polygon ng-click="click(\'bottom\')" points="15,35 35,35 50,50 0,50" style="fill:{{color[teeth.bottom.fillColor]}};stroke:black;stroke-width:{{teeth.bottom.strokeWidth}}"/><polygon ng-click="click(\'center\')" points="15,35 35,35 35,15 15,15" style="fill:{{color[teeth.center.fillColor]}};stroke:black;stroke-width:{{teeth.center.strokeWidth}}" /> <polygon points="5,0 25,25 5,50 25,25 45,0 25,25 45,50" style="stroke:black;stroke-width:4;display:{{teeth.X ? \'\' : \'none\' }}" ng-click="teeth.X = !teeth.X"/> <polygon points="7.5,0 25,50 42.5,0 25,50 7.5,0" style="stroke:black;stroke-width:4;display:{{teeth.V ? \'\' : \'none\' }}" ng-click="teeth.V = !teeth.V"/>  <polygon ng-show="teeth.fracture" ng-click="teeth.fracture = !teeth.fracture" points="10,17.5 40,17.5" style="stroke:black;stroke-width:3" ></polygon> <polygon  ng-click="teeth.fracture = !teeth.fracture" ng-show="teeth.fracture"points="10,32.5 40,32.5" style="stroke:black;stroke-width:3" ></polygon> <polygon  ng-click="teeth.fracture = !teeth.fracture" ng-show="teeth.fracture" points="20,7.5 12.5,42.5" style="stroke:black;stroke-width:3" ></polygon> <polygon ng-click="teeth.fracture = !teeth.fracture" ng-show="teeth.fracture" points="37.5,7.5 30,42.5" style="stroke:black;stroke-width:3" ></polygon>  <polygon ng-show="teeth.fmc" ng-click="teeth.fmc = !teeth.fmc" points="0,0 0,50 50,50 50,0 0,0 50,0 50,50 0,50 0,0" style="fill:white;stroke:black;stroke-width:5"/>  </svg> <div ng-show="labelPos == \'bottom\'" style="height:20px">{{teeth.erupt}}</div><div  ng-show="labelPos == \'bottom\'" style="height:20px">{{label}}</div><svg  ng-show="labelPos == \'top\'" width="50" height="25"><polygon points="12.5,0 25,25, 37.5,0, 12.5,0" style="fill:{{teeth.rct ? \'black\' : \'white\'}};stroke:{{teeth.rct || teeth.nvt ? \'black\' : \'white\'}};stroke-width:1"></polygon> <polygon ng-show="teeth.arrowRight" ng-click="teeth.arrowRight = !teeth.arrowRight" points="10,12.5 40,12.5 38,10 38,15 40,12.5 " style="stroke:black;stroke-width:1"/>  <polygon ng-show="teeth.arrowLeft" ng-click="teeth.arrowLeft = !teeth.arrowLeft" points="40,12.5 10,12.5 12,10 12,15 10,12.5 " style="stroke:black;stroke-width:1"/> </svg> </div>',
        templateUrl: './js/directives/teeth.html',
        link: function(scope, element, attrs){
          scope.init = function(){
            scope.teeth = {
              mis:false, 
              rrx:false, 
              nvt:false,
              rct:false, 
              erupt:'', 
              fracture:false, 
              fmc:false, 
              arrowRight:false, 
              arrowLeft:false
            }
            scope.teeth.actions = []
            scope.teeth.left = {
              fillColor: 0,
              strokeWidth: 1
            }
            scope.teeth.top = {
              fillColor: 0,
              strokeWidth: 1
            }
            scope.teeth.bottom = {
              fillColor: 0,
              strokeWidth: 1
            }
            scope.teeth.right = {
              fillColor: 0,
              strokeWidth: 1
            }
            scope.teeth.center = {
              fillColor: 0,
              strokeWidth: 1
            }
          }
          if(!scope.teeth){
            scope.init()
          }
          scope.color = [
            'white',
            'black',
            'green',
            'brown',
            'url(#lightstripe)',
            'url(#redstripe)'
          ]
          scope.click = function(pos){
            // console.log(scope.action)
            // console.log(scope.teeth[pos].fillColor, pos)
            switch (scope.action) {
              case 'amf':
                scope.teeth.center.fillColor = (scope.teeth.center.fillColor == 1) ? 0 : 1
                break
              case 'car':
                scope.teeth[pos].strokeWidth = (scope.teeth[pos].strokeWidth == 4) ? 0 : 4
                break
              case 'cof':
                scope.teeth[pos].fillColor = (scope.teeth[pos].fillColor == 4) ? 0 : 4
                break
              case 'fis':
                scope.teeth[pos].fillColor =  (scope.teeth[pos].fillColor == 5) ? 0 : 5
                break
              case 'nvt':
                scope.teeth.nvt = !scope.teeth.nvt
                break
              case 'rct':
                scope.teeth.rct = !scope.teeth.rct
                break
              case 'non':
                scope.teeth.erupt = (scope.teeth.erupt == "NON") ? "" : "NON"
                break
              case 'une':
                scope.teeth.erupt = (scope.teeth.erupt == "UNE") ? "" : "UNE"
                break
              case 'pre':
                scope.teeth.erupt = (scope.teeth.erupt == "PRE") ? "" : "PRE"
                break
              case 'ano':
                scope.teeth.erupt = (scope.teeth.erupt == "ANO") ? "" : "ANO"
                break
              case 'prd':
                scope.teeth.erupt = (scope.teeth.erupt == "PRD") ? "" : "PRD"
                break
              case 'ipx':
                scope.teeth.erupt = (scope.teeth.erupt == "IPX") ? "" : "IPX"
                break
              case 'fld':
                scope.teeth.erupt = (scope.teeth.erupt == "FLD") ? "" : "FLD"
                break
              case 'fcr':
                scope.teeth.fracture = !scope.teeth.fracture
                break
              case 'rrx':
                scope.teeth.rrx = !scope.teeth.rrx
                break
              case 'mis':
                scope.teeth.mis = !scope.teeth.mis
                break
              case 'fmc':
                scope.teeth.fmc = !scope.teeth.fmc
                break
              case 'meb':
                scope.teeth.fmc = true
                break
              case 'poc':
                scope.teeth.fmc = true
                scope.teeth.left.fillColor = 4
                scope.teeth.right.fillColor = 4
                scope.teeth.center.fillColor = 4
                scope.teeth.bottom.fillColor = 4
                scope.teeth.top.fillColor = 4
                break
              case 'rotL':
                scope.teeth.arrowLeft = !scope.teeth.arrowLeft
                scope.teeth.arrowRight = false
                break
              case 'rotR':
                scope.teeth.arrowRight = !scope.teeth.arrowRight
                scope.teeth.arrowLeft = false
                break
              case 'sou':
                scope.init()
                break
              default:
                break
            }
            if(scope.clickCallback){
              scope.clickCallback(scope.posX,scope.posY,scope.labelPos)
            }
          }
          scope.setFill = function(pos, fill){
          }
        }
    };
  });
  OdontogramApp.directive('teethAction', function(){
    return {
      restrict: 'AEC',
      transclude:true,
      scope: {
        action: '='
      },
      template: '<div class="list-group"><button ng-repeat="(k, v) in actions track by $index" ng-click="setAction(k)" class="list-group-item {{action == k ? \'active\' : \'\'}}" >{{k + \' - \' + v}}</button></div>',
      link: function(scope,element,attrs){
        scope.setAction = function(v) {
          // console.log(v)
          scope.action = v
        }
        if(!scope.action) scope.action = 'amf'
        scope.actions = {
          // "cls": "",
          "sou": "Normal/ baik",
          "amf": "Tambalan Amalgam",
          "cof": "Tambalan Composite",
          "fis": "Pit dan fi ssure sealant",
          "nvt": "gigi non-vital",
          "rct": "Perawatan Saluran Akar",
          "non": "Gigi tidak ada, tidak diketahui ada atau tidak ada",
          "une": "Un-Erupted",
          "pre": "Partial Erupt",
          "ano": "Anomali",
          "car": "Caries = Tambalan sementara",
          "fcr": "Fracture",
          "fmc": "Full metal crown pada gigi vital",
          "poc": "Porcelain crown pada gigi vital",
          "rrx": "Sisa Akar",
          "meb": "Full metal bridge",
          "mis": "Gigi hilang",
          "ipx": "Implant",
          "prd": "Partial Denture",
          "fld": "Full Denture",
          "rotL": "Migrasi/ Version/Rotasi Kiri",
          "rotR": "Migrasi/ Version/Rotasi Kanan"
        }
      }
    }
  })