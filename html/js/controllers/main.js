bigboxappController.controller("mainController",
function($window,$scope,$http){
    $scope.loading = false
    $scope.params = {
        msisdn: '',
        text: ''
    }
    $scope.send = function() {
        $scope.$applyAsync(function(){
            $scope.loading=true
            $http.defua
            $http.post('../', $scope.params, {headers:{
                'Content-Type': 'application/json'
            }}).then(function(response){
                $scope.loading=false
            }).catch(function(error){
                $scope.loading=false
            })
        })
    }
})