OdontogramController.controller("odontogram",
function($window,$scope,$http){
    $scope.teethTop=[{label:"18",teeth:null,type:"square"},{label:"17",teeth:null,type:"square"},{label:"16",teeth:null,type:"square"},{label:"15",teeth:null,type:"square"},{label:"14",teeth:null,type:"square"},{label:"13",teeth:null,type:"center"},{label:"12",teeth:null,type:"center"},{label:"11",teeth:null,type:"center"},{label:"21",teeth:null,type:"center"},{label:"22",teeth:null,type:"center"},{label:"23",teeth:null,type:"center"},{label:"24",teeth:null,type:"square"},{label:"25",teeth:null,type:"square"},{label:"26",teeth:null,type:"square"},{label:"27",teeth:null,type:"square"},{label:"28",teeth:null,type:"square"}],$scope.teethTopCenter=[{label:"55",teeth:null,type:"square"},{label:"54",teeth:null,type:"square"},{label:"53",teeth:null,type:"center"},{label:"52",teeth:null,type:"center"},{label:"51",teeth:null,type:"center"},{label:"61",teeth:null,type:"center"},{label:"62",teeth:null,type:"center"},{label:"63",teeth:null,type:"center"},{label:"64",teeth:null,type:"square"},{label:"65",teeth:null,type:"square"}],$scope.teethBottomCenter=[{label:"85",teeth:null,type:"square"},{label:"84",teeth:null,type:"square"},{label:"83",teeth:null,type:"center"},{label:"82",teeth:null,type:"center"},{label:"81",teeth:null,type:"center"},{label:"71",teeth:null,type:"center"},{label:"72",teeth:null,type:"center"},{label:"73",teeth:null,type:"center"},{label:"74",teeth:null,type:"square"},{label:"75",teeth:null,type:"square"}],$scope.teethBottom=[{label:"48",teeth:null,type:"square"},{label:"47",teeth:null,type:"square"},{label:"46",teeth:null,type:"square"},{label:"45",teeth:null,type:"square"},{label:"44",teeth:null,type:"square"},{label:"43",teeth:null,type:"center"},{label:"42",teeth:null,type:"center"},{label:"41",teeth:null,type:"center"},{label:"31",teeth:null,type:"center"},{label:"32",teeth:null,type:"center"},{label:"33",teeth:null,type:"center"},{label:"34",teeth:null,type:"square"},{label:"35",teeth:null,type:"square"},{label:"36",teeth:null,type:"square"},{label:"37",teeth:null,type:"square"},{label:"38",teeth:null,type:"square"}];
    $scope.meb = []
    $scope.mebStat = 'from'
    $scope.mebFromPosY = ''
    $scope.mebFromPosX = ''
    $scope.action = 'meb'
    $scope.removeMEB = function(pos){
        if($scope.mebStat == 'to') $scope.mebStat = 'from'
        $scope.meb.splice(pos,1)
    }
    $scope.teethClick = function(X,Y,labelPos){
        if ($scope.action != 'meb') return
        
        if($scope.mebStat == 'from'){
            $scope.mebFromPosY = Y
            $scope.mebFromPosX = X
            var points = []
            if (labelPos == 'bottom'){
                var fX = X + 25
                var fY = Y + 75
                points.push(fX + ","+ fY)
                points.push(fX + ","+ (fY + 10))     
            } else {
                var fX = X + 25
                var fY = Y + 30
                points.push(fX + ","+ (fY + 10))     
                points.push(fX + ","+ fY)
            
            }
            $scope.meb.push({points:points})
            $scope.mebStat = 'to'
        } else {

            if ($scope.mebFromPosY == Y && $scope.mebFromPosX == X  ) {
                return 
            }
            if ($scope.mebFromPosY != Y) {
                alert('Posisi MEB harus sejajar')
                return 
            }
            if (labelPos == 'bottom'){ 
                var fX = X + 25
                var fY = Y + 75
                $scope.meb[$scope.meb.length -1].points.push(fX + ","+ (fY + 10))  
                $scope.meb[$scope.meb.length -1].points.push(fX + ","+ fY) 
                $scope.meb[$scope.meb.length -1].points.push(fX + ","+ (fY + 10))
                $scope.meb[$scope.meb.length -1].points.push($scope.meb[$scope.meb.length -1].points[1])

            } else {
                var fX = X + 25
                var fY = Y + 30
                $scope.meb[$scope.meb.length -1].points.push(fX + ","+ fY) 
                $scope.meb[$scope.meb.length -1].points.push(fX + ","+ (fY + 10))  
                $scope.meb[$scope.meb.length -1].points.push(fX + ","+ fY) 
                $scope.meb[$scope.meb.length -1].points.push($scope.meb[$scope.meb.length -1].points[1])
    
            }
            $scope.mebStat = 'from'
        }

        console.log(X,Y,labelPos)
    }
})