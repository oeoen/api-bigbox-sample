const express = require('express')
const app = express()
const port = 3000
const axios = require('axios')
const bodyparser = require('body-parser')
const API_KEY = 'B0jZ63iewgl3pjlstGazg2wdKVs5suON'
const BIGBOX_HOST = 'https://api.thebigbox.id/SMSNotification/1.0.0'
const SENDER_ID = 'TELKOM'
const qs = require('qs')
app.use(bodyparser.json())
app.get('/', (req, res) => res.send('Hello World!'))
app.post('/', async(r, w) => {
  try{
    resp = await axios.post(BIGBOX_HOST + '/messages', qs.stringify({
        msisdn: r.body.msisdn,
        content: r.body.text
      }), {
      headers: {
        'X-API-KEY': API_KEY,
        'X-MainAPI-Senderid': SENDER_ID,
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
    console.log(resp)
    w.send({message: 'success'})
  } catch(ex) {
  console.log(ex)
    w.status(500).send({message: 'error'})
  }
})
app.use('/app', express.static('html'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))